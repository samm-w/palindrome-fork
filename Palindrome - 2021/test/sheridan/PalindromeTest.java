package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PalindromeTest {

	@Test
	public void testIsPalindromeRegular( ) {
		String word = "dad";
		assertTrue("Word is not a palindrome", Palindrome.isPalindrome(word));
	}

	@Test
	public void testIsPalindromeException( ) {
		String word = "dog";
		assertFalse("Word is not a palindrome", Palindrome.isPalindrome(word));
	}
	@Test
	public void testIsPalindromeBoundaryIn( ) {
		String word = "edit tide";
		assertTrue( "not a palindrome.", Palindrome.isPalindrome(word) );
	}
	@Test
	public void testIsPalindromeBoundaryOut( ) {
		String word = "edit on tide";
		assertFalse("Word is not a palindrome", Palindrome.isPalindrome(word));
	}	
	
}
